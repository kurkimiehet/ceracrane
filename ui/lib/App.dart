import 'package:flutter/material.dart';
import 'package:sample/LandingPage.dart';
import 'package:sample/OperatorView.dart';
import 'dart:async';

enum TabItem { LandingPage, OperatorView }

final navigatorKey = GlobalKey<NavigatorState>();

class App extends StatefulWidget {
  App({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _App createState() => _App();
}

class _App extends State<App> {
  bool switched = false;

  LandingPage landingPage;
  OperatorView operatorView;
  List<Widget> pages;
  Widget currentPage;

  Timer switchDebouncer;
  List<bool> isSelected;

  @override
  void initState() {
    landingPage = LandingPage();
    operatorView = OperatorView();

    pages = [landingPage, operatorView];
    isSelected = [true, false];

    currentPage = landingPage;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: currentPage,
      bottomSheet: BottomAppBar(
        color: const Color.fromRGBO(250, 250, 250, 1.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              height: 95,
              padding: EdgeInsets.fromLTRB(0, 0, 30, 30),
              child: Column(
                children: <Widget>[
                  ToggleButtons(
                    borderColor: Color.fromRGBO(0, 0, 0, 0.5),
                    borderWidth: 1,
                    selectedBorderColor: Color.fromRGBO(0, 0, 0, 0.5),
                    selectedColor: Color.fromRGBO(0, 0, 0, 0.8),
                    color: Colors.grey,
                    fillColor: Color(0xffe15554),
                    borderRadius: BorderRadius.circular(4),
                    children: <Widget>[
                      Container(
                          width: 100,
                          child: Icon(
                            Icons.search,
                            size: 30.0,
                          )),
                      Opacity(
                        opacity: (isSelected[0]) ? 0.4 : 1.0,
                        child: Container(
                          width: 100,
                          alignment: Alignment.centerRight,
                          height: 30,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: new AssetImage("res/crane2.png"),
                            ),
                          ),
                        ),
                      )
                    ],
                    onPressed: (int index) {
                      if (switchDebouncer != null) {
                        // blocked
                        return;
                      }
                      switchDebouncer = Timer(Duration(milliseconds: 3000), () {
                        switchDebouncer = null;
                      });
                      setState(() {
                        for (int i = 0; i < isSelected.length; i++) {
                          isSelected[i] = i == index;
                          currentPage = pages[isSelected[0] == true ? 0 : 1];
                        }
                      });
                    },
                    isSelected: isSelected,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
