import 'dart:core';

import 'package:flutter/material.dart';
import 'Constants.dart' as Constants;

class WaterfallLine extends StatelessWidget {
  final int id;
  final int jobStatus;
  WaterfallLine({this.id, this.jobStatus});
  @override
  Widget build(BuildContext context) {
    return Text(
      Constants.JOB_STATUS[id],
      style: TextStyle(
          fontWeight: (jobStatus == id ? FontWeight.w900 : FontWeight.w100)),
    );
  }
}
