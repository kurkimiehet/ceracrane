from db import CeraDB
import unittest

class TestDB(unittest.TestCase):

    def test_insert_read(self):
        db = CeraDB()
        db.insert_weighing_record("Ilmatar",
                                  5.234, 
                                  5.234,
                                  "1997-09-14 23:04:34",
                                  "CERA7428ILM899271-37290",
                                  "jkldsjakd8u3828d32jiodj8jaoijdijaoid8d8329d",
                                  "XMLXMLXML",
                                  "CGJRNYKIKSQSNAVXEPKFSQVNORUZMOORCTJZJHUYEWSPSCXSVYNMLZUWBYCKFSMNDAGXFTIBIVXYTC999",
                                  "DCC_XMLHERE"
                                  )
        criteria = {"container_id": "CERA7428ILM899271-37290"}
        records = db.fetch_weighing_records(criteria)
        #assert len(records) == 1 #TODO: For some reason finding more than 1 record, possibly init.sql or old records?
        assert len(records) != 0

if __name__ == "__main__":
    unittest.main()

