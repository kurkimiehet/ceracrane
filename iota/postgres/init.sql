DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
COMMENT ON SCHEMA public IS 'The standard schema for ceracrane db';

CREATE TABLE measurements (
    crane_name text,                    --name of the operating crane (i.e. 'Ilmatar')
    load_gross_t float(32) NOT NULL,    --the gross weight of container
    load_tared_t float(32),             --tared weight of container
    date_time varchar(32),              --type varchar avoids conversion issues
    container_id text NOT NULL,         --container id
    fingerprint text NOT NULL,          --the fingerprint calculated for the container
    xml_string text,                    --value read from an xml file
    iota_tx_hash text NOT NULL,
    dcc text                            --digital calibration certificate for the crane
);

INSERT INTO measurements (crane_name, load_gross_t, load_tared_t, date_time,
    container_id, fingerprint, xml_string, iota_tx_hash, dcc)
VALUES (
    'Ilmatar', 
    3.478, 
    0.5, 
    '1997-09-14 23:04:34', 
    'CERA7428ILM899271-37281', 
    'VlGQpd59g3mssf1Bc47tHdnpC3ebuhxg83zFMDLRMc1k0',
    'xn0GHEmW1jEN2pxDSIICa7a4bZSpFnUcPS1klkAInV79fGaGKvdAwSPGxgjgx8Fa3FUD44jeq2IfrrhZdQZHeg8BJPQkadLYUFyZC2wrnz69D1vS3vh0QGipCNTXhTjw1aUlqcQ5g27NxbrseSmnbUjkMoisijp4eiu9sAJCC9kQgW11rqxh6EZfZi1NrBtliT7HOklyaHlMrbxD6EUgxxUAICATNtpCvUVUeLHudgpNZdTpVbKVHeacatAB8T9g',
    'YC9TAVPFYFIYXLMCJT9ZINYJNYPSUTXLXEMJMZDHTXTAAATTSMKFDOBZCNOPRKEIMHDBBTMSW9OJWR999',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    3.02, 
    1.10, 
    '1998-09-14 23:04:34', 
    'CERA7428ILM899271-37282', 
    'gUXntTyb1jrKKyWBbLWgZ7j0nwNPypfmtkNh3tjUFgYQg',
    'NVCOVpT8b9DVkTpSTiX8jbMHMiZA7t5gs9x3JyncITOUVtk7QE4w5eAaEv29PoRZga3EFvpJssESUl9dDj960P0mvgm4xQkREZj3yARfCBn4RyTWcjQojrBzoGhQpfY5htqB4iKTd6TFxXS3IKhkNGIjG6jruhTFOTp8KZljdcLtkAg0wXoVujQnm3wS93Ygpy2Y8qz8B0QW2rYZLnL37mm87DrzmiySQnC3LSjhIvh3mQbYuZwmxYGbXENDNh9L',
    'CGJRNYKIKSQSNAVXEPKFSQVNORUZMOORCTJZJHUYEWSPSCXSVYNMLZUWBYCKFSMNDAGXFTIBIVXYTC999',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    3.52, 
    1.10, 
    '1998-09-14 23:05:45', 
    'CERA7428ILM899271-37282', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
),
(
    'Ilmatar', 
    2.11, 
    1.10, 
    '1999-09-14 23:05:45', 
    'C99999999', 
    'Nf4JvNydhfbKmqAgeX2IsFe1WeOyqBYVZge4sxfOoxeEO',
    'L7QLJ90ZHiNKCFevlTpmXqP3G2PixM9wxqRec23pDjQVfttcV7Kpsd1WZz4N2AFRCK1NzdquNLVikS1lHAd9NSAhcrfb9vHKLWb6pOapYSKCW22azo9144hbDeDkguehp9Uf6NYAFMxPfvaA1HW60fiurOH8MI9EIOulOCpyKgPiPxRcguUKBo7EyKCsjFW3c0eKqDGSo1yov4IzYg1GoooWUgA8pY4dqFZrkDG2SQZko32vOLZpTiNtvJjeg1aF',
    'GTIBIVX9YTC99SQCSNAVXEPKFSQVNOJRNYKSPMSCXSVYNMLZUWBYCKFSMNDIKRUZOORCTJZJHUYEWAGXF',
    'EXAMPLE-DCC-STRING'
);

