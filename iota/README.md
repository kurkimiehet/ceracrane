# IOTA/Database

The Python scripts in this folder perform three functions:

1.  Set up the PostgeSQL database.
2.  Provide a REST API for writing and reading to/from Iota and the
    database.
3.  Provide the underlying writing and reading functionality.

## IOTA/DB REST

The REST offers the following endpoints:

**Get a measurement's signed XML file:** `GET db-rest/api/xml`

Arguments: \
 `tx` - The Iota transaction hash which identifying the measurement

Returns: \
 A String representing the measurement's XML

**Get measurement data for a container:** `GET db-rest/api/read`

Arguments: \
 `id` - The id of the container whose measurements are to be fetched

Returns: \
 A JSON list of JSON objects with the structure:

    [
     {
      "crane_name": ,
      "load_gross_t": ,
      "load_tared_t": ,
      "container_id": ,
      "datetime": ,
      "xml": ,
      "iota_tx_hash": ,
      "dcc": ,
      "iota_confirmed": 
     },
     {
      "crane_name": ,
      "load_gross_t": ,
      "load_tared_t": ,
      "container_id": ,
      "datetime": ,
      "xml": ,
      "iota_tx_hash": ,
      "dcc": ,
      "iota_confirmed": 
     },
     ...
    ]

For definitions of the JSON object's fields, see the section on
[PostgreSQL](#postgresql).

**Store a measurement to Iota and the database:**
`POST db-rest/api/write`

Arguments: \
 `payload` - A JSON with the structure

    {
     "data": { 
              "crane_name":
              "load_gross_t":
              "load_tared_t":
              "container_id":
              "datetime":
              "xml":
              "dcc":
              "iota_confirmed":
             },
     "signature_hash": ,
     "xmlstr": ,
     "dccstr": 
    }

Returns: \
 A JSON indicating the status of the writing thread with structure

    {
     "status": ,
     "job_id": ,
     "iscomplete": ,
     "hasfailed":
    }

**Get the status of a measurement storing operation:**
`POST db-rest/api/getstatus`

Arguments: \
 `id` - The job id of the thread responsible for writing a measurement
(the field "job\_id" in the response of the `write` endpoint).

Returns: \
 A JSON indicating the status of the writing thread with structure

    {
     "status": ,
     "job_id": ,
     "iscomplete": ,
     "hasfailed":
    }

## PostgreSQL

The PostgreSQL database consists of one table, `measurements`, with the
following columns

-   `crane_name` - The name of the crane weighing the container and
    signing the measurement
-   `load_gross_t` - Gross load weighed
-   `load_tared_t` - Tared load weighed
-   `date_time` - Date and time of the measurement
-   `container_id` - The id of the weighed container
-   `fingerprint` - The fingerprint of the signed measurement XML
-   `xml_string` - The measurement XML
-   `iota_tx_hash` - The hash of the Iota transaction containing
    measurement's information
-   `dcc` - The digital calibration certificate of the crane

For more information about, e.g., the types of the columns, see
[postgres/init.sql](postgres/init.sql).

## Iota

IOTA is a distributed ledger technology (DLT) that allows devices in an
IOTA network to transact in micropayments and to send each other
immutable data. For more information about Iota, see [Iota
Documentation](https://docs.iota.org/).

### Iota addresses for the project

When the `db-rest` service is run, `iotaconf.py` generates multiple Iota
addresses based on the seed specified in the file and selects one of
them to be used. The addresses are stored in
[python\_scripts/iota\_addresses.txt](/python_scripts/iota_addresses.txt).

A new address to use can be selected manually in `iotaconf.py` by
changing the variable `ADDRESS`.

Note that normally each Iota address is used only one transaction. Hence
the current project, being a proof-of-concept, does not use Iota
addresses correctly as all the transactions are stored in the same
address.

### Iota comnet Tangle

The project uses a development Iota Tangle called `comnet` to store
measurement information. An explorer for the network is available at
https://comnet.thetangle.org/.

The transactions of a specific address can be accessed by appendind
'address/[iota-address]' to the URL, e.g.
https://comnet.thetangle.org/address/JXHSCFLMOMMXUOVHPUMENJZHJHHTIOYLVDVZIDJRLMFHAUDLBSZROJWTYOWMVJCQMVWLXYLVUETXETHC9CHSDXXHID.

### Iota transactions

Each Iota transaction has fields called tag and message/signature (just
message in the following).

As the tag the API uses the hex of a 6-byte Blake2B hash of the container's id.
Tangle can then be searched for the hash of the desired container id.
The use of hash allows arbitrarily long container ids to be used
although Iota limits the length of the tag to 12 bytes.

The message of the transaction is essentially a ciphertext calculated
from the measurement data. For details, see the section on [Algorithm
for validating DB data against
Iota](#algorithm-for-validating-db-data-against-iota).

## Misc

### Algorithm for validating DB data against Iota

When reading measurement data from the database through the REST API,
the data is validated against Iota to confirm it has not been modified
since storing it. Below is a short descipription how the validation
algorithm works.

The algorithm relies on two assumptions:

1.  The key used for Fernet cipher is stored securely
2.  Data written to Iota is immutable.

When data is stored

1.  A JSON string is created from the measurement data: \
     `j = JSON(measurement-data)`
2.  The JSON string is hashed with SHA-256: \
     `h = SHA256(j)`
3.  The SHA-256 hash is prepended with a prespecified tag (currently
    ’CERA’): \
    ` p = 'CERA' + h`
4.  The resulting string, `p`, is encrypted with a Fernet cipher which
    is uses AES in CBC-mode: \
     `c = fernet_enc(key, p)`
5.  An Iota transaction is created. The tag of the transaction is a
    6-bit Blake2B hash of the container id. The message of the
    transaction is the ciphertext created in the previous step, `c`. The
    transaction is stored in the Tangle.
6.  The measurement data accompanied by the Iota’s transaction hash is
    stored in the database.

When data is retrieved, its integrity is checked as follows:

1.  The container id whose measurements are to be read is passed as a
    parameter to the REST API.
2.  The 6-byte Blake2B hash is calculated from the container id. The
    Tangle is searched for all transaction having the hash as their tag.
3.  The message, `m`, of each found transactions is decrypted with
    Fernet cipher: \
     `q = fernet_dec(key, m)`
4.  The transactions are validated by checking their decrypted message,
    `q` begins with the prespecified tag, `CERA`: \
     `q[0:4] = 'CERA'`
5.  A set `I` is formed of the valid transaction messages: \
     `I = { q : q[0:4] = 'CERA' }` \
6.  A set `D` of all the measurements in the database with the given
    container id is formed.
7.  The sets `I` and `D` must equal in size. If not, none of the
    measurements in the database is confirmed.
8.  If the sizes of the sets are equal, a SHA-256 hash, `h'`, is
    calculated for each measurement in the database and prepended with
    the prespecified tag, `r = 'CERA' + h'`.
9.  A measurement in the database is confirmed if the resulting string
    is found in the set of valid Iota transactions: \
     `r in I` \
     Otherwise the measurement is marked as unconfirmed.
10. A list of all the measurements in the database with the given
    container id is returned from the API. The list indicates for each
    measurement whether it was confirmed or not.

The above algorithm counters the following attacks:

-   Modifying a record: Tag+SHA256-hash, `r`, not found in the valid
    Iota transactions, `I`, when reading from the database
-   Removing a record: The number of records in Iota, `|I|`, and
    database, `|D|`, do not match
-   Adding a record: The number of records in Iota, `|I|`, and database,
    `|D|`, do not match
-   Removing a record and adding a new one: Tag+SHA256-hash, `r`, of the
    new record not found in valid Iota transactions `I`
-   Adding an Iota transaction with the correct Iota transaction tag but
    nonsensical message: Decrypted message of the added transaction does
    not begin with the prespecified tag, (`q[0:4] != 'CERA'`), so the
    transaction is not used for confirming measurements in the database

 
