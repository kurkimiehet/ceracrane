import sys
sys.path.append('../')
import unittest
from opclient import opclient
from lxml import etree
from validation import signatureIsValid
import sys
import os
from pathlib import Path
import time
import json
import time

class TestClient(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        address = "opc.tcp://opcua-mockup:4840/freeopcua/server/"
        self.client = opclient(address)
        fn = str(Path(__file__).resolve().parent) + '/xml/measurementEvent.xsd'
        self.xmlschema = etree.XMLSchema(etree.parse(fn))

    @classmethod
    def tearDownClass(self):
        pass

    def test_connection(self):
        crane = self.client._connect_to_crane()
        self.assertEqual(crane is not None, True, "Connection to the server failed")
        crane.disconnect()

    def test_XML(self):
        data = self.client.fetchData()
        xml = self.client.constructXML(data)
        self.assertEqual(self.xmlschema.validate(xml), True, "XML format was not correct")
    
    def test_data(self):
        data_1 = self.client.fetchData()
        self.assertEqual(data_1 is not None, True, "Fetching data packet 1 failed.")
        time.sleep(4)
        data_2 = self.client.fetchData()
        self.assertEqual(data_2 is not None, True, "Fetching data packet 2 failed.")
        self.assertEqual(data_1['load_gross'] == data_2['load_gross'], False, "Load_gross values are the same. Have you hardcoded something?")
        self.assertEqual(data_1['load_tared'] == data_2['load_tared'], False, "Load_gross_tared values are the same. Have you hardcoded something?")
        self.assertEqual(data_1['datetime'].split(":")[2].split("+")[0] == data_2['datetime'].split(":")[2].split("+")[0], False, "Time hasn't elapsed between measurements. Have you hardcoded something?")
    

    def test_validation(self):
        def warmup_rest():
            print("warming up xades")
            data = self.client.fetchData()
            xml = self.client.constructXML(data)
            xml_original = etree.tostring(xml).decode()
            tmp = self.client.signData(xml_original, "http://xades-signer:3000/sign")
            print("se lepää", flush=True)
            time.sleep(2)
            print("...")
            time.sleep(5)
            print("...")
            time.sleep(5)
            print("...")
            time.sleep(5)
            print("...")
            time.sleep(5)
            print("warmup done", flush=True)
        warmup_rest()
        data = self.client.fetchData()
        xml = self.client.constructXML(data)
        xml_original = etree.tostring(xml).decode()
        tmp = self.client.signData(xml_original, "http://xades-signer:3000/sign")
        self.assertEqual(tmp is not None, True, "Signed data is null, is the REST & Xades signers running?")
        tmp = etree.tostring(tmp).decode()
        testing = False
        i = 0
        while (i < 4):
            try:
                testing = signatureIsValid(tmp, "http://eidas-rest:8080/services/rest/validation/validateSignature")
                break
            except:
                print("algebruh retrying... ," + str(i+1), flush=True)
                i += 1

        self.assertEqual(testing, True, "Signature was not valid")

if __name__ == '__main__':
    unittest.main()
