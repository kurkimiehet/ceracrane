from flask import Flask, json, request
from opclient import opclient
from lxml import etree
from validation import signatureIsValid
from threading import Thread
from flask_cors import CORS, cross_origin
import requests
import uuid
import time
import os, sys

api = Flask(__name__)
cors = CORS(api)
api.config['CORS_HEADERS'] = 'Content-Type'
status = {}

DCCREF = "DCC_BX-10504-CAL.xml"

def error(msg):
    return json.dumps({'message': msg})

@api.route('/cranestatus', methods=['GET'])
@cross_origin()
def weightFromCrane():
    address = "opc.tcp://opcua-mockup:4840/freeopcua/server"
    if "crane_address" in os.environ:
        address = os.getenv('crane_address') #ENV Variable "crane_address" which changes the address the rest connects to, DEFAULT DOCKER
    client = opclient(address)
    data = client.fetchData()
    ret = {
        'weight': data['load_gross'],
        'tared': data['load_tared'],
        'datetime': data['datetime']
    }
    return json.dumps(ret)

@api.route('/validate', methods=['POST'])
@cross_origin()
def validate():
    payload = request.get_json(force=True)
    xml_string = ""
    try:
        xml_string = payload['xmlstr']
    except:
        return error("JSON payload did not have element 'xmlstr'")
    ret = signatureIsValid(xml_string)
    return json.dumps({'signatureIsValid': ret})

# TAKES PARAMETER "id"
@api.route('/status', methods=['GET'])
@cross_origin()
def get_status():
    if request.args.get('id') is None:
        return error("Invalid job id")
    jobid = str(request.args.get('id'))
    try:
        ret = status[jobid]
        return json.dumps(ret)
    except:
        ret = {'message': "Invalid job id"}
        return json.dumps(ret)

# TAKES PARAMETER "id"
@api.route('/createjob')
@cross_origin()
def request_job():
    if request.args.get('id') is None:
        return error("Container ID was not given")
    containerId = str(request.args.get('id'))
    jobid = str(uuid.uuid4())
    status[jobid] = {
        "status": "Job created",
        "statusnum": 0,
        "jobid" : jobid,
        "data": {},
        "xml": ""
    }
    thread = Thread(target=start_job, kwargs={'jobid': jobid, 'containerId': containerId})
    thread.start()
    return json.dumps(status[jobid])

def start_job(jobid, containerId):
    #Get data from crane
    crane_address = "opc.tcp://opcua-mockup:4840/freeopcua/server"
    if "crane_address" in os.environ:
        address = os.getenv('crane_address')
    client = opclient(crane_address)
    data = client.fetchData(containerId=containerId)
    status[jobid]['status'] = "Data fetched from the crane"
    status[jobid]['statusnum'] = 1
    status[jobid]['data'] = data
    dcc_str = DCCREF

    #Sign XML
    xmlstr_original = etree.tostring(client.constructXML(data)).decode()
    xades_address = "http://xades-signer:3000/sign"
    if "xades_address" in os.environ:
        address = os.getenv('xades_address')
    xml_signed = client.signData(xmlstr_original, url=xades_address) #ENV Variable "xades_address" changes address to sign REST-api, DEFAULT DOCKER
    xml_string = etree.tostring(xml_signed).decode()
    signature = parseSignature(xml_signed)
    status[jobid]['status'] = "XML created and signed"
    status[jobid]['statusnum'] = 2
    status[jobid]['xml'] = xml_string
    
    #Validate signature
    eidas_address = "http://eidas-rest:8080/services/rest/validation/validateSignature"
    if "eidas_address" in os.environ:
        eidas_address = (os.getenv('eidas_address') + "/services/rest/validation/validateSignature") #ENV Variable "eidas_address" changes address&port to eidas validate
    if signatureIsValid(xml_string, eidas_address):
        status[jobid]['status'] = "XML signature was valid"
        status[jobid]['statusnum'] = 3
    else:
        status[jobid]['status'] = "XML signature was not valid. Job failed."
        status[jobid]['statusnum'] = -1
        print("Job " + jobid + " failed")
        return

    #Send to database and IOTA
    status[jobid]['status'] = "Waiting for data to be written to IOTA and database"
    status[jobid]['statusnum'] = 4
    db_payload = {
        'data': data,
        'xmlstr': xml_string,
        'dccstr': dcc_str,
        'signature_hash': signature
    }
    dbrest_address = "http://db-rest:9010"
    if "dbrest_address" in os.environ:               #ENV Variable "dbrest_address" changes address to db/iota rest-api, DEFAULT DOCKER
        address = os.getenv('dbrest_address')
    db_jobresponse = json.loads(requests.post(url=(dbrest_address + "/api/write"), json=db_payload).text)
    db_jobid = db_jobresponse['job_id']
    db_status = json.loads(requests.get(url=(dbrest_address + "/api/getstatus"), params={'id': db_jobid}).text)
    while not db_status['iscomplete']:
        time.sleep(5)
        db_status = json.loads(requests.get(url=(dbrest_address + "/api/getstatus"), params={'id': db_jobid}).text)
    if db_status['hasfailed']:
        status[jobid]['status'] = "Writing to IOTA and database failed."
        status[jobid]['statusnum'] = -1
        print("Job " + jobid + " failed")
        return

    #Update status to complete
    status[jobid]['status'] = "Job is complete"
    status[jobid]['statusnum'] = 5
    print("Job " + jobid + " completed")

def parseSignature(xml):
    signature = xml.find('{http://www.w3.org/2000/09/xmldsig#}Signature')
    signatureValue = signature.find('{http://www.w3.org/2000/09/xmldsig#}SignatureValue').text
    return signatureValue

if __name__ == '__main__':
    api.run(host='0.0.0.0', port='9000', debug=True)
