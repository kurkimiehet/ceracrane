# This service is obsolete!
This service is a modified version of the original [DSS project](https://github.com/esig/dss) that works with Gradle and can only sign XAdES signatures.

**This service is not needed! Please refer to [this project](https://github.com/AaltoSmartCom/dss-demonstrations) and [this documentation](https://github.com/AaltoSmartCom/dss-demonstrations/tree/master/documentation) if you wish to create (XAdES) signatures.**


## Usage
Creating the keys:
```console
gradle run --args='hello.xml user_a_rsa.p12 ebin'
```
Building the image and running the service:
```console
docker build -t signer . && docker run -p 3000:3000 signer && node sender.js
```